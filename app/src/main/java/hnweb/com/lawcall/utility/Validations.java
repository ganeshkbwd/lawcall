package hnweb.com.lawcall.utility;

import android.text.TextUtils;
import android.util.Patterns;

/**
 * Created by neha on 12/29/2016.
 */

public class Validations {
    public static boolean strslength(String word) {
        return word.length() > 0 && !TextUtils.isEmpty(word);
    }

    public static boolean emailCheck(String email) {
        return strslength(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean confirmPass(String password, String confirmPass) {
        return strslength(confirmPass) && password.matches(confirmPass);
    }
}
