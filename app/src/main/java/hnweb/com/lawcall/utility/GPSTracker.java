package hnweb.com.lawcall.utility;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.util.List;

/**
 * Created by neha on 9/26/2016.
 */

public class GPSTracker {

    public static Location myLocation(Context context) {


        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

// // Create a criteria object to retrieve provider
// Criteria criteria = new Criteria();
//
// // Get the name of the best provider
//// String provider = locationManager.getBestProvider(criteria, true);

        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            Location l = locationManager.getLastKnownLocation(provider);
//            Log.e("LOCATION", l.getLatitude()+" "+l.getLongitude());
// Location l = locationManager.getLastKnownLocation(provider);

            if (l == null) {
                continue;
            }
            if (bestLocation == null
                    || l.getAccuracy() < bestLocation.getAccuracy()) {
                Log.d("found best last", l.toString());
                bestLocation = l;
            }
        }
        if (bestLocation == null) {
            return null;
        }

        return bestLocation;
// Get Current Location
// Location myLocation = locationManager.getLastKnownLocation(provider);
// return myLocation;

    }


    public static boolean isLocationEnabled(final Context context) {

        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
//        boolean gps_enabled = false;
        boolean network_enabled = false;

//        try {
//            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
//        } catch (Exception ex) {
//        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        return network_enabled;

    }

}
