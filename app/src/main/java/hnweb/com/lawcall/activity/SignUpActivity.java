package hnweb.com.lawcall.activity;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hnweb.com.lawcall.application.MainApplication;
import hnweb.com.lawcall.utility.CheckConnectivity;
import hnweb.com.lawcall.utility.LoadingDialog;
import hnweb.com.lawcall.R;
import hnweb.com.lawcall.utility.ToastUlility;
import hnweb.com.lawcall.utility.Validations;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    EditText fullNameET, emailET, phoneET, cityET, passwordET, repasswordET;
    private LoadingDialog loadingDialog;
    String name1 = "", name2 = "", name3 = "", phone1 = "", phone2 = "", phone3 = "";
    TextView contact1, contact2, contact3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        fullNameET = (EditText) findViewById(R.id.fullNameET);
        emailET = (EditText) findViewById(R.id.emailET);
        phoneET = (EditText) findViewById(R.id.phoneET);
        cityET = (EditText) findViewById(R.id.cityET);
        passwordET = (EditText) findViewById(R.id.passwordET);
        repasswordET = (EditText) findViewById(R.id.repasswordET);
        contact1 = (TextView) findViewById(R.id.conatact1);
        contact2 = (TextView) findViewById(R.id.conatact2);
        contact3 = (TextView) findViewById(R.id.conatact3);

        contact1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectContact(1);
            }
        });
        contact2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectContact(2);
            }
        });
        contact3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectContact(3);
            }
        });

        loadingDialog = new LoadingDialog(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.continueIB:
                checkValid();
                break;
            case R.id.fbIB:
                ToastUlility.show(this, "Coming soon.........");
                break;
            case R.id.loginIB:
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    public void checkValid() {
        if (!Validations.strslength(fullNameET.getText().toString().trim()) &&
                !Validations.strslength(emailET.getText().toString().trim()) &&
                !Validations.strslength(phoneET.getText().toString().trim()) &&
                !Validations.strslength(cityET.getText().toString().trim()) &&
                !Validations.strslength(passwordET.getText().toString().trim()) &&
                !Validations.strslength(repasswordET.getText().toString().trim())) {
            fullNameET.setError("Please enter full name.");
            fullNameET.requestFocus();
            emailET.setError("Please enter email address.");
            phoneET.setError("Please enter phone no.");
            cityET.setError("Please enter city or zipcode.");
            passwordET.setError("Please enter password.");
            repasswordET.setError("Please enter confirm pasword.");

        } else if (!Validations.strslength(fullNameET.getText().toString().trim())) {
            fullNameET.setError("Please enter full name.");
            fullNameET.requestFocus();
        } else if (!Validations.emailCheck(emailET.getText().toString().trim())) {
            emailET.setError("Please enter valid email address.");
            emailET.requestFocus();
        } else if (!Validations.strslength(phoneET.getText().toString().trim())) {
            phoneET.setError("Please enter phone no.");
            phoneET.requestFocus();
        } else if (!Validations.strslength(cityET.getText().toString().trim())) {
            cityET.setError("Please enter city or zipcode.");
            cityET.requestFocus();
        } else if (!Validations.strslength(passwordET.getText().toString().trim())) {
            passwordET.setError("Please enter password.");
            passwordET.requestFocus();
        } else if (!Validations.confirmPass(passwordET.getText().toString().trim(), repasswordET.getText().toString().trim())) {
            repasswordET.setError("Confirm pasword and password are not match.");
            repasswordET.requestFocus();
        } else {
            if (CheckConnectivity.checkInternetConnection(this)) {
                doRegister();
            } else {
                CheckConnectivity.noNetMeg(this);
            }

//            ToastUlility.show(this, "Coming soon.....");
        }
    }


    public void doRegister() {
        loadingDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/designer321.com/johnram/lawcall/index.php/api/register", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("VERIFY RESPONSE", response.toString());

                try {
                    JSONObject jobj = new JSONObject(response);
                    int message_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");

                    if (loadingDialog.isShowing()) {
                        loadingDialog.dismiss();
                    }
                    if (message_code == 1) {
                        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    ToastUlility.show(SignUpActivity.this, message);

                } catch (JSONException e) {
                    if (loadingDialog.isShowing()) {
                        loadingDialog.dismiss();
                    }
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("full_name", fullNameET.getText().toString().trim());
                params.put("email_address", emailET.getText().toString().trim());
                params.put("phone_no", phoneET.getText().toString().trim());
                params.put("city_zipcode", cityET.getText().toString().trim());
                params.put("password", passwordET.getText().toString().trim());
                params.put("name1", name1);
                params.put("phone1", phone1);
                params.put("name2", name2);
                params.put("phone2", phone2);
                params.put("name3", name3);
                params.put("phone3", phone3);

                Log.e("PARAMS", params.toString());
                return params;

            }
        };
        String request_tag = "user_register";
//        queue.add(stringRequest);

        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }


    private void selectContact(int i) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
        startActivityForResult(intent, i);

//        Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:0377778888"));
//        smsIntent.putExtra("sms_body", "sms message goes here");
//        startActivity(smsIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("reco", String.valueOf(requestCode));

        Cursor cursor = null;
        String phoneNumber = "", primaryMobile = "";

        List<String> allNumbers = new ArrayList<String>();
        int contactIdColumnId = 0, phoneColumnID = 0, nameColumnID = 0;
        try {
            Uri result = data.getData();
            Log.e("TAG", result.toString());
            String id = result.getLastPathSegment();
            cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);
            contactIdColumnId = cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID);
            phoneColumnID = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA);
            nameColumnID = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);

            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    String idContactBook = cursor.getString(contactIdColumnId);
                    String displayName = cursor.getString(nameColumnID);
                    phoneNumber = cursor.getString(phoneColumnID);

                    if (phoneNumber.length() == 0)
                        continue;

                    int type = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                    if (type == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE && primaryMobile.equals(""))
                        primaryMobile = phoneNumber;
                    allNumbers.add(phoneNumber);
//                    for (int i=0;i<allNumbers.size();i++){
                    Log.e("PHONR", phoneNumber + idContactBook + displayName);

                    if (requestCode == 1) {
                        name1 = displayName;
                        phone1 = phoneNumber;
                        contact1.setText(name1 + "\n" + phone1);
                    } else if (requestCode == 2) {
                        name2 = displayName;
                        phone2 = phoneNumber;
                        contact2.setText(name2 + "\n" + phone2);
                    } else if (requestCode == 3) {
                        name3 = displayName;
                        phone3 = phoneNumber;
                        contact3.setText(name3 + "\n" + phone3);
                    }
//                    }

                    cursor.moveToNext();
                }
            } else {
                // no results actions
            }
        } catch (Exception e) {
            // error actions
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}

