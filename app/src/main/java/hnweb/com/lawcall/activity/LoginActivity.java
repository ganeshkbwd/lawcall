package hnweb.com.lawcall.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import hnweb.com.lawcall.application.MainApplication;
import hnweb.com.lawcall.utility.CheckConnectivity;
import hnweb.com.lawcall.utility.LoadingDialog;
import hnweb.com.lawcall.R;
import hnweb.com.lawcall.utility.ToastUlility;
import hnweb.com.lawcall.utility.Validations;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText emailET, passwordEt;
    private LoadingDialog loadingDialog;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailET = (EditText) findViewById(R.id.emailET);
        passwordEt = (EditText) findViewById(R.id.passwordET);
        loadingDialog = new LoadingDialog(this);

        prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
        editor = prefs.edit();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.registerBTN:
                Intent intent = new Intent(this, SignUpActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.forgotTV:

                forgotPasswordDialog();

                break;
            case R.id.signinBTN:
                if (!Validations.strslength(emailET.getText().toString().trim()) &&
                        !Validations.strslength(passwordEt.getText().toString().trim())) {
                    emailET.setError("Please enter valid email address.");
                    emailET.requestFocus();
                    passwordEt.setError("Please enter password");

                } else if (!Validations.emailCheck(emailET.getText().toString().trim())) {
                    emailET.setError("Please enter valid email address.");
                    emailET.requestFocus();

                } else if (!Validations.strslength(passwordEt.getText().toString().trim())) {
                    passwordEt.setError("Please enter password");
                    passwordEt.requestFocus();

                } else {
                    if (CheckConnectivity.checkInternetConnection(this)) {
                        doLogin();
                    } else {
                        CheckConnectivity.noNetMeg(this);
                    }

//                    Intent intent1 = new Intent(this, CallActivity.class);
//                    startActivity(intent1);
//                    finish();
//                    ToastUlility.show(this, "Under development.......");
                }
                break;
        }
    }

    public void doLogin() {
        loadingDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/designer321.com/johnram/lawcall/index.php/api/login_user", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("VERIFY RESPONSE", response.toString());

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int message_code = jsonObject.getInt("message_code");
                    String message = jsonObject.getString("message");
                    ToastUlility.show(LoginActivity.this, message);
                    if (message_code == 1) {

                        JSONObject jarr = jsonObject.getJSONObject("info");
                        String user_id = jarr.getString("pkey");
                        String name = jarr.getString("name");
                        String email = jarr.getString("email");
                        String phone = jarr.getString("phone");
                        String city = jarr.getString("city");

                        editor.putString("USER_ID", user_id);
                        editor.putString("NAME", name);
                        editor.putString("EMAIL", email);
                        editor.putString("PHONE", phone);
                        editor.putString("CITY", city);
                        editor.commit();

                        Intent intent1 = new Intent(LoginActivity.this, CallActivity.class);
                        startActivity(intent1);
                        finish();

                    } else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("email_address", emailET.getText().toString().trim());
                params.put("password", passwordEt.getText().toString().trim());

                Log.e("PARAMS", params.toString());
                return params;

            }
        };
        String request_tag = "user_login";
//        queue.add(stringRequest);

        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

    public void forgotPasswordDialog() {
        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.forgot_password
                , null));
        settingsDialog.setCancelable(true);
        final EditText fppassTV = (EditText) settingsDialog.findViewById(R.id.fp_email_idET);
        Button submitBTN = (Button) settingsDialog.findViewById(R.id.fpsubmitBTN);
        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fppassTV.getText().toString().trim().length() > 0 && Patterns.EMAIL_ADDRESS.matcher(fppassTV.getText().toString().trim()).matches()) {
                    String email = fppassTV.getText().toString().trim();
                    if (CheckConnectivity.checkInternetConnection(LoginActivity.this)) {
                        doForgotPassword(email, settingsDialog);
                    } else {
                        CheckConnectivity.noNetMeg(LoginActivity.this);
                    }

                } else {

                    Toast.makeText(LoginActivity.this, "Please enter your correct email id", Toast.LENGTH_SHORT).show();
                }
            }
        });

        settingsDialog.show();
    }

    public void doForgotPassword(final String email, final Dialog settingsDialog) {

        loadingDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/designer321.com/johnram/lawcall/index.php/api/forgot_password", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("VERIFY RESPONSE", response.toString());

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int message_code = jsonObject.getInt("message_code");
                    String message = jsonObject.getString("message");
                    ToastUlility.show(LoginActivity.this, message);
                    if (message_code == 1) {

                        settingsDialog.dismiss();

                    } else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email_address", email);
                Log.e("PARAMS", params.toString());
                return params;

            }
        };
        String request_tag = "user_login";
//        queue.add(stringRequest);

        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);

    }
}
