package hnweb.com.lawcall.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import hnweb.com.lawcall.application.MainApplication;
import hnweb.com.lawcall.utility.CheckConnectivity;
import hnweb.com.lawcall.utility.LoadingDialog;
import hnweb.com.lawcall.R;
import hnweb.com.lawcall.utility.TelephonyInfo;
import hnweb.com.lawcall.utility.ToastUlility;
import hnweb.com.lawcall.utility.TrackGPS;

public class CallActivity extends AppCompatActivity implements View.OnClickListener {

    protected LocationManager locationManager;
    protected LocationListener locationListener;
    ImageView callIV;
    Toolbar toolbar;
    TextView contact1, contact2, contact3;
    String finalAddress = "";

    SharedPreferences sharedPreferences;
    String client_name, client_email, user_id;
    String name1 = "", name2 = "", name3 = "", phone1 = "", phone2 = "", phone3 = "";
    double longitude;
    double latitude;
    private LoadingDialog loadingDialog;
    private TrackGPS gps;
    String IPaddress;
    Boolean IPValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(LocationServices.API)
//                .build();
        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        client_name = sharedPreferences.getString("NAME", "");
        client_email = sharedPreferences.getString("EMAIL", "");
        user_id = sharedPreferences.getString("USER_ID", "");
        loadingDialog = new LoadingDialog(this);
        GPS();

        NetwordDetect();


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.callIV:

                if (CheckConnectivity.checkInternetConnection(this)) {
                    doSendEmailToLawyer();
                } else {
                    ToastUlility.show(this, "Please check your intenet connection and try again.");
                }


                break;


        }
    }

    public void setAvailTimeDialog() {

        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.confirm_your_call_dialog
                , null));
        settingsDialog.setCancelable(false);


        ImageView closeBTN = (ImageView) settingsDialog.findViewById(R.id.closeBTN);
        closeBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingsDialog.dismiss();
            }
        });

        ImageView shareIV = (ImageView) settingsDialog.findViewById(R.id.shareIV);
        shareIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareItWhatsApp();
            }
        });

        final ImageView smsIV = (ImageView) settingsDialog.findViewById(R.id.smsIV);
        smsIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                sendSMS();
                selectUser();
                settingsDialog.dismiss();
            }
        });

        final ImageView callIV = (ImageView) settingsDialog.findViewById(R.id.callIV);
        callIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCall();
                settingsDialog.dismiss();
//                gotoConfirm();
            }
        });
//        ListView timeLV = (ListView) settingsDialog.findViewById(R.id.appointmentScheduleLV);
//        Button bookBTN = (Button) settingsDialog.findViewById(R.id.bookBTN);
//        Button cancelBTN = (Button) settingsDialog.findViewById(R.id.cancelBTN);
//        ConstantVal.selectedPosition = -1;
//
//        timeLV.setAdapter(new TimingsAdapter(availableTimings, TherapistProfileActivity.this));
//        cancelBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                settingsDialog.dismiss();
//            }
//        });
//
//        bookBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (ConstantVal.selectedPosition != -1) {
//                    if (login == 1) {
//                        settingsDialog.dismiss();
//
//                        bookingConfirmDialog(filelist, position + "", availableTimings, ConstantVal.selectedPosition + "");
//
//                    } else {
//
//                        ConstantVal.myFileList = filelist;
//                        ConstantVal.therapistPosition = String.valueOf(position);
//                        ConstantVal.myAvialtime = availableTimings;
//
//                        Intent intent = new Intent(TherapistProfileActivity.this, SignInActivity.class);
//                        intent.putExtra("ComeFrom", "Book");
//                        startActivity(intent);
//                        finish();
//                        settingsDialog.dismiss();
//                    }
//
//                } else {
//
//                    Toast.makeText(TherapistProfileActivity.this, "Please select appointment time.", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });


        settingsDialog.show();


    }

    public void gotoConfirm() {
        Intent intent = new Intent(CallActivity.this, ConfirmationActivity.class);
        startActivity(intent);
    }

    public void selectUser() {

        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.choose_user
                , null));
        settingsDialog.setCancelable(false);

        Button lawyerBTN = (Button) settingsDialog.findViewById(R.id.lawyerBTN);
        Button lovedBTN = (Button) settingsDialog.findViewById(R.id.lovedBTN);
        ImageView closeBTN = (ImageView) settingsDialog.findViewById(R.id.closeBTN);

        lawyerBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                sendSMS();

                String details = "Name :- " + client_name + "\nEmail Id :- " + client_email + "\nCell No. :- " + sharedPreferences.getString("PHONE", "") + "\nLocation :- " + finalAddress + "\nIP Address :- " + IPaddress;
                String message = client_name + "has been detained by police at " + finalAddress + ". " + client_name + " requests your legal services immediately." + "\n\n" + details;
//                phoneNumber = contact1.getText().toString().trim() + ";" + contact1.getText().toString().trim() + ";" + contact1.getText().toString().trim();
                sendSMS("5128393343", message);
            }
        });

        lovedBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                selectContact(contact1);
                doGetContact();

            }
        });

        closeBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingsDialog.dismiss();
            }
        });

        settingsDialog.show();
    }

    private void shareItWhatsApp() {
//sharing implementation here
        boolean isAppInstalled = appInstalledOrNot("com.whatsapp");
        String details = "Name :- " + client_name + "\nEmail Id :- " + client_email + "\nCell No. :- " + sharedPreferences.getString("PHONE", "") + "\nLocation :- " + finalAddress + "\nIP Address :- " + IPaddress;
        String message = client_name + "has been detained by police at " + finalAddress + ". " + client_name + " requests your legal services immediately." + "\n\n" + details;

        if (isAppInstalled) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.setPackage("com.whatsapp");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "AndroidSolved");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);
            startActivityForResult(Intent.createChooser(sharingIntent, "Share via"), 102);
//        \startActivity(Intent.createChooser(sharingIntent, "Share via"));
        } else {
            ToastUlility.show(CallActivity.this, "WhatsApp is currently not installed in your phone.");
        }

    }

    private void selectContact(int i) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
        startActivityForResult(intent, i);

//        Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:0377778888"));
//        smsIntent.putExtra("sms_body", "sms message goes here");
//        startActivity(smsIntent);
    }

    public void contactDialog(String name1, String phone1, String name2, String phone2, String name3, String phone3) {

        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.contact_list
                , null));
        settingsDialog.setCancelable(false);
        contact1 = (TextView) settingsDialog.findViewById(R.id.contact1TV);
        contact2 = (TextView) settingsDialog.findViewById(R.id.contact2TV);
        contact3 = (TextView) settingsDialog.findViewById(R.id.contact3TV);


        if (phone1.equalsIgnoreCase("")) {
            contact1.setText("Please select contact 1");
        } else {
            contact1.setText(name1 + "\n" + phone1);
        }
        if (phone2.equalsIgnoreCase("")) {
            contact2.setText("Please select contact 2");
        } else {
            contact2.setText(name2 + "\n" + phone2);
        }
        if (phone3.equalsIgnoreCase("")) {
            contact3.setText("Please select contact 3");
        } else {
            contact3.setText(name3 + "\n" + phone3);
        }

        Button sendBTn = (Button) settingsDialog.findViewById(R.id.sendBTN);
        Button cancelBTN = (Button) settingsDialog.findViewById(R.id.cancelBTN);

        contact1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectContact(1);
            }
        });
        contact2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectContact(2);
            }
        });
        contact3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectContact(3);
            }
        });

        sendBTn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = "";

                String details = "Name :- " + client_name + "\nEmail Id :- " + client_email + "\nCell No. :- " + sharedPreferences.getString("PHONE", "") + "\nLocation :- " + finalAddress + "\nIP Address :- " + IPaddress;
                String message = client_name + " has been detained by police at+" + finalAddress + "." + "\n\n" + details;

//                String message = client_name + "has been detained by police at " + finalAddress + ". " + client_name + " requests your legal services immediately.\nCell No. - " + sharedPreferences.getString("PHONE", "") + "\nIP :- " + IPaddress + "\nEmail :- " + sharedPreferences.getString("EMAIL", "");

                String c1, c2, c3;
                c1 = CallActivity.this.phone1.trim();
                c2 = CallActivity.this.phone2.trim();
                c3 = CallActivity.this.phone3.trim();
                if (!TextUtils.isEmpty(c1)) {
                    sendSMS(c1, message);
                }
                if (!TextUtils.isEmpty(c2)) {
                    sendSMS(c2, message);
                }
                if (!TextUtils.isEmpty(c3)) {
                    sendSMS(c3, message);
                }

                if (CheckConnectivity.checkInternetConnection(CallActivity.this)) {
                    doAddContact(settingsDialog);
                } else {
                    CheckConnectivity.noNetMeg(CallActivity.this);
                }


//                phoneNumber = c1 + c2 + c3;

            }
        });

        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingsDialog.dismiss();
            }
        });


        settingsDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 101) {
            Intent intent = getIntent();
            finish();
            startActivity(intent);

        } else if (requestCode == 102) {
            gotoConfirm();
        } else {
            Log.e("reco", String.valueOf(requestCode));

            Cursor cursor = null;
            String phoneNumber = "", primaryMobile = "";

            List<String> allNumbers = new ArrayList<String>();
            int contactIdColumnId = 0, phoneColumnID = 0, nameColumnID = 0;
            try {
                Uri result = data.getData();
                Log.e("TAG", result.toString());
                String id = result.getLastPathSegment();
                cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);
                contactIdColumnId = cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID);
                phoneColumnID = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA);
                nameColumnID = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);

                if (cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {
                        String idContactBook = cursor.getString(contactIdColumnId);
                        String displayName = cursor.getString(nameColumnID);
                        phoneNumber = cursor.getString(phoneColumnID);

                        if (phoneNumber.length() == 0)
                            continue;

                        int type = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        if (type == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE && primaryMobile.equals(""))
                            primaryMobile = phoneNumber;
                        allNumbers.add(phoneNumber);
//                    for (int i=0;i<allNumbers.size();i++){
                        Log.e("PHONR", phoneNumber + idContactBook + displayName);

                        if (requestCode == 1) {
                            name1 = displayName;
                            phone1 = phoneNumber;
                            contact1.setText(name1 + "\n" + phone1);
                        } else if (requestCode == 2) {
                            name2 = displayName;
                            phone2 = phoneNumber;
                            contact2.setText(name2 + "\n" + phone2);
                        } else if (requestCode == 3) {
                            name3 = displayName;
                            phone3 = phoneNumber;
                            contact3.setText(name3 + "\n" + phone3);
                        }
//                    }

                        cursor.moveToNext();
                    }
                } else {
                    // no results actions
                }
            } catch (Exception e) {
                // error actions
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }

    private void makeCall() {
        PhoneCallListener phoneListener = new PhoneCallListener();
        TelephonyManager telephonyManager = (TelephonyManager) this
                .getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneListener,
                PhoneStateListener.LISTEN_CALL_STATE);


        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:0377778888"));

        if (ActivityCompat.checkSelfPermission(CallActivity.this,
                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
//        startActivityForResult(callIntent, 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
//        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logout) {
            logout(this);
//            Intent intent = new Intent(this, LoginActivity.class);
//            startActivity(intent);
//            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void GPS() {

        gps = new TrackGPS(CallActivity.this);


        if (gps.canGetLocation()) {


            longitude = gps.getLongitude();
            latitude = gps.getLatitude();

            if (latitude != 0 && longitude != 0) {
                getAddress(latitude, longitude);
            } else {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
//                ToastUlility.show(CallActivity.this, "Please restart app and try again.");
            }
//
        } else {
            showSettingsAlert();
        }



    }


    public void getAddress(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            finalAddress = address + "," + city + "," + state + "," + country + "," + postalCode;
            Log.e("ADRESS", finalAddress);



        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public void checkGPS() {
        try {
            int off = Settings.Secure.getInt(CallActivity.this.getContentResolver(), Settings.Secure.LOCATION_MODE);

            if (off == 0) {
                AlertDialog.Builder dialog_bulder = new AlertDialog.Builder(CallActivity.this);
                dialog_bulder.setTitle("GPS Location");
                dialog_bulder.setMessage("Please Turn on GPS");
                dialog_bulder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.cancel();
                        Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(onGPS, 100);
                    }
                });
                dialog_bulder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.cancel();

                    }
                });
                AlertDialog dialog = dialog_bulder.create();
                dialog.show();
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }


    private void sendSMS(String phoneNumber, String message) {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(CallActivity.this, 0, new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(CallActivity.this, 0, new Intent(DELIVERED), 0);

// ---when the SMS has been sent---
        CallActivity.this.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(CallActivity.this, "SMS sent", Toast.LENGTH_SHORT).show();
                        gotoConfirm();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        dualSim();
//                        Toast.makeText(CallActivity.this, "Generic failure", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(CallActivity.this, "No service", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(CallActivity.this, "Null PDU", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(CallActivity.this, "Radio off", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

// ---when the SMS has been delivered---
        CallActivity.this.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(CallActivity.this, "SMS delivered", Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(CallActivity.this, "SMS not delivered", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }

    //dual Sim
    private void dualSim() {

        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(CallActivity.this);
        boolean isSIM1Ready = telephonyInfo.isSIM1Ready();
        boolean isSIM2Ready = telephonyInfo.isSIM2Ready();
        boolean isDualSIM = telephonyInfo.isDualSIM();

        Log.e("IS DUAL SIM", "" + isDualSIM);
        Log.e("isSIM1Ready", "" + isSIM1Ready);
        Log.e("isSIM2Ready", "" + isSIM2Ready);

        if (isDualSIM) {
            if (isSIM1Ready || isSIM2Ready) {
                Toast.makeText(CallActivity.this, "An Unexpected failure occured while sending SMS. Please check whether you have working SMS plan and try again later.",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(CallActivity.this, "Please activate atleast one SIM for sending SMS and retry.",
                        Toast.LENGTH_LONG).show();
            }
        } else {
            if (!isSIM1Ready) {
                Toast.makeText(CallActivity.this, "No SIM detected to perform this action.",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(CallActivity.this, "An Unexpected failure occured while sending SMS. Please check whether you have working SMS plan and try again later.",
                        Toast.LENGTH_LONG).show();
            }
        }

    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

    public void doSendEmailToLawyer() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/designer321.com/johnram/lawcall/index.php/api/send_mail_to_lowyer", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("VERIFY RESPONSE", response.toString());

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int message_code = jsonObject.getInt("message_code");
                    String message = jsonObject.getString("message");
                    ToastUlility.show(CallActivity.this, message);
                    if (message_code == 1) {
                        setAvailTimeDialog();

                    } else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("user_id", sharedPreferences.getString("USER_ID", ""));
                params.put("ip_address", IPaddress);
                params.put("location", finalAddress);

                Log.e("PARAMS", params.toString());
                return params;

            }
        };
        String request_tag = "mail_to_lawyer";
//        queue.add(stringRequest);

        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

    public void doAddContact(final Dialog settingsDialog) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/designer321.com/johnram/lawcall/index.php/api/add_contact_of_user", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("VERIFY RESPONSE", response.toString());

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int message_code = jsonObject.getInt("message_code");
                    String message = jsonObject.getString("message");
//                    ToastUlility.show(CallActivity.this, message);
                    if (message_code == 1) {
                        settingsDialog.dismiss();


                    } else {
                        ToastUlility.show(CallActivity.this, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("user_id", user_id);
                params.put("name1", name1);
                params.put("phone1", phone1);
                params.put("name2", name2);
                params.put("phone2", phone2);
                params.put("name3", name3);
                params.put("phone3", phone3);

                Log.e("PARAMS", params.toString());
                return params;

            }
        };
        String request_tag = "mail_to_lawyer";
//        queue.add(stringRequest);

        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

    public void doGetContact() {
        loadingDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/designer321.com/johnram/lawcall/index.php/api/get_user_contact", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("VERIFY RESPONSE", response.toString());

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int message_code = jsonObject.getInt("message_code");

                    if (message_code == 1) {
                        JSONObject message = jsonObject.getJSONObject("message");
                        name1 = message.getString("name1");
                        phone1 = message.getString("phone1");
                        name2 = message.getString("name2");
                        phone2 = message.getString("phone2");
                        name3 = message.getString("name3");
                        phone3 = message.getString("phone3");


                        contactDialog(name1, phone1, name2, phone2, name3, phone3);

                    } else {
                        String message = jsonObject.getString("message");
                        ToastUlility.show(CallActivity.this, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("user_id", user_id);

                Log.e("PARAMS", params.toString());
                return params;

            }
        };
        String request_tag = "get_user_contact";
//        queue.add(stringRequest);

        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

    public void showSettingsAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(CallActivity.this);


        alertDialog.setTitle("GPS Not Enabled");

        alertDialog.setMessage("Do you wants to turn On GPS");


        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 101);
            }
        });


        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        alertDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gps.stopUsingGPS();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    //phone all state
    private class PhoneCallListener extends PhoneStateListener {

        String LOG_TAG = "LOGGING 123";
        private boolean isPhoneCalling = false;

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            if (TelephonyManager.CALL_STATE_RINGING == state) {
                // phone ringing
                Log.i(LOG_TAG, "RINGING, number: " + incomingNumber);
            }

            if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
                // active
                Log.i(LOG_TAG, "OFFHOOK");

                isPhoneCalling = true;
            }

            if (TelephonyManager.CALL_STATE_IDLE == state) {
                // run when class initial and phone call ended, need detect flag
                // from CALL_STATE_OFFHOOK
                Log.i(LOG_TAG, "IDLE");

                if (isPhoneCalling) {

                    Log.i(LOG_TAG, "restart app");

                    // restart app
                    gotoConfirm();
                    isPhoneCalling = false;
                }

            }
        }
    }

    public static void logout(final Activity context) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context,
                        SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
//                toastDialog(context, "You are logout successfully");
                Toast.makeText(context,
                        "You are logout successfully", Toast.LENGTH_LONG).show();
                SharedPreferences settings = context.getApplicationContext()
                        .getSharedPreferences(context.getPackageName(),
                                Context.MODE_PRIVATE);
                settings.edit().clear().commit();
                context.finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();


    }

    //Check the internet connection.
    private void NetwordDetect() {

        boolean WIFI = false;

        boolean MOBILE = false;

        ConnectivityManager CM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();

        for (NetworkInfo netInfo : networkInfo) {

            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))

                if (netInfo.isConnected())

                    WIFI = true;

            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))

                if (netInfo.isConnected())

                    MOBILE = true;
        }

        if (WIFI == true)

        {
            IPaddress = GetDeviceipWiFiData();
            Log.e("IPWIFI", IPaddress);
//            textview.setText(IPaddress);


        }

        if (MOBILE == true) {

            IPaddress = GetDeviceipMobileData();
            Log.e("IPPHONE", IPaddress);
//            textview.setText(IPaddress);

        }

    }


    public String GetDeviceipMobileData() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                 en.hasMoreElements(); ) {
                NetworkInterface networkinterface = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = networkinterface.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
//                        return inetAddress.getHostAddress().toString();
                        return ip;

                    }
                }
            }
        } catch (Exception ex) {
            Log.e("Current IP", ex.toString());
        }
        return null;
    }

    public String GetDeviceipWiFiData() {

        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);

        @SuppressWarnings("deprecation")

        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

        return ip;

    }

}

