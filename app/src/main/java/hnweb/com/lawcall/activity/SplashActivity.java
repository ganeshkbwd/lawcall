package hnweb.com.lawcall.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import hnweb.com.lawcall.R;
import hnweb.com.lawcall.utility.PermissionUtility;
import hnweb.com.lawcall.utility.TrackGPS;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    public static double myLocationLatitude;
    public static double myLocationLongitude;
    SharedPreferences sharedPreferences;
    Location location;
    double longitude;
    double latitude;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private Intent bundle;
    private LocationManager mgr;
    private String best;
    private TrackGPS gps;
    private PermissionUtility putility;
    private ArrayList<String> permission_list;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        editor = prefs.edit();
        permissions();

    }

    @Override
    public void onClick(View view) {
        if (sharedPreferences.getString("USER_ID", "").equalsIgnoreCase("")) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, CallActivity.class);
            startActivity(intent);
            finish();
        }


    }


    public void permissions() {
        putility = new PermissionUtility(this);
        permission_list = new ArrayList<String>();
        permission_list.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        permission_list.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permission_list.add(android.Manifest.permission.INTERNET);
        permission_list.add(android.Manifest.permission.ACCESS_WIFI_STATE);
        permission_list.add(android.Manifest.permission.ACCESS_NETWORK_STATE);
        permission_list.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permission_list.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        permission_list.add(android.Manifest.permission.READ_CONTACTS);
        permission_list.add(android.Manifest.permission.CAMERA);
        permission_list.add(android.Manifest.permission.SEND_SMS);
        permission_list.add(Manifest.permission.READ_SMS);
        permission_list.add(Manifest.permission.READ_CONTACTS);
        permission_list.add(Manifest.permission.RECEIVE_SMS);
        permission_list.add(Manifest.permission.BROADCAST_SMS);
        permission_list.add(Manifest.permission.READ_PHONE_STATE);

        putility.setListner(new PermissionUtility.OnPermissionCallback() {
            @Override
            public void OnComplete(boolean is_granted) {
                Log.i("OnPermissionCallback", "is_granted = " + is_granted);
                if (is_granted) {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
//go next

//                            goNextScreen();
                        }
                    }, 3000);

                } else {
                    putility.checkPermission(permission_list);
                }
            }
        });


        putility.checkPermission(permission_list);
    }

}
